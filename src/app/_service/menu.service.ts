import { Menu } from './../_model/menu';
import { HttpClient } from '@angular/common/http';
import { HOST } from './../_shared/var.constant';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private url: string = `${HOST}`;

  constructor(private http: HttpClient) { }

  listar(){
    return this.http.get<Menu[]>(`${this.url}/menus`);
  }
}
